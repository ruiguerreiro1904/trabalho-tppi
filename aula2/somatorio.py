##
# \brief Somatório de números entre um intervalo
# \details Aula 2
# \author Rui Guerreiro  
# \date 27/11/2019
# \bug 0 bugs
# \version 0.0  

## Valor que armazena o número inferior introduzida pelo utilizador
infer = int(input("Introduza o número inferior"))
## Valor que armazena o número superior introduzida pelo utilizador
super = int(input("Introduza o número superior"))

## Definir o contador para começar no valor que o utilizador inseriu
cont = infer
## Variável que vai armazenar o resultado final
res = 0
while cont <= super:
   res = res + cont
   cont+=1

print(f" O somatório é {res}")
