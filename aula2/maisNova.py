##
# \brief Saber a pessoa mais nova
# \details Aula 2
# \author Rui Guerreiro  
# \date 27/11/2019
# \bug 0 bugs
# \version 0.0    


## Valor que vai armazenar a idade mais nova
idadeMinima = 500

while True:
    ## Valor que armazena o bi introduzido pelo utilizador
    bi = input("Introduza a número do BI")
    if bi!='0':
        ## Valor que armazena o nome introduzido pelo utilizador
        nome = input("Introduza o nome")
        ## Valor que armazena a idade introduzido pelo utilizador
        idade = int(input("Introduza a idade"))

        if idade < idadeMinima:
            ## Se a idade atual for menor que a idade mínima, então a idade mínima passa a ser a idade
            idadeMinima = idade
            ## O mesmo para o nome
            nomeMin = nome
    else:
        print(f"A pessoa mais nova chama-se {nomeMin} e tem {idadeMinima}")
        break






