##
# \brief Factoração de um número 
# \details Aula 2
# \author Rui Guerreiro  
# \date 27/11/2019
# \bug 0 bugs
# \version 0.0  


## Armazena o número introduzido pelo utilizador
num = int(input('Introduza um número'))

## Elemento da factoração
i = 1
if num == 0:
     print('0')
elif num == 1:
    print('1')
else:
     ## Elemento para saber quando é resto zero
    fator = 2
    while num != 1:
        while num % fator == 0:
            ##  Váriavel que guarda todos os fatores
            num = num / fator
            print(fator)
        ## Contador  
        fator = fator + 1
