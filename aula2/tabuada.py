##
# \brief Tabuada de um número
# \details Aula 2
# \author Rui Guerreiro  
# \date 27/11/2019
# \bug 0 bugs
# \version 0.0


## Valor que armazena o número da tabuada pretendida pelo utilizador
num = int(input("Introduza o numero da tabuada pretendida"))

## Variável que vai fazer de contador (1, 2, 3, 4...)
cont=1
## Variável que vai armazenar o resultado final
res = 0

while cont<=10:
   res = num * cont
   print(f" {num} * {cont} = {res}")
   cont+=1
