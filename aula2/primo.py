##
# \brief Saber se o número é primo
# \details Aula 2
# \author Rui Guerreiro  
# \date 27/11/2019
# \bug 0 bugs
# \version 0.0    

## Valor que armazena o número introduzido pelo utilizador
num = int(input("Introduza um número"))

if num > 1:
    for i in range (2,num):
        if (num % i) == 0:
            print(f"O numero {num} não é primo")
            break
    else:
        print(f"O numero {num} é primo")

else:
    print(f"O numero {num} não é primo")
