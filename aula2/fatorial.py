##
# \brief Fatorial de um número
# \details Aula 2
# \author Rui Guerreiro  
# \date 27/11/2019
# \bug 0 bugs
# \version 0.0    


## Valor que armazena o número introduzido
num = int(input("Introduza um número"))

## Fatorial
fatorial = 1

if num < 0:
    print("Não é possivel apresentar a fatorização")

elif num==0:
    print("O fatorial do número 0 é 1")
else:
    for i in range(1,num+1):
        ## Guarda o resultado de cada multiplicação
        fatorial = fatorial*i
print("O fatorial do número",num,"é",fatorial)
