##
# \brief Distância entre dois pontos 
# \details Aula 2
# \author Rui Guerreiro  
# \date 27/11/2019
# \bug 0 bugs
# \version 0.0    


## Valor de x do primeiro ponto
x1 = int(input("Introduza x1"))
## Valor de y do primeiro ponto
y1 = int(input("Introduza y1"))
## Valor de x do segundo ponto
x2 = int(input("Introduza x2"))
## Valor de y do segundo ponto
y2 = int(input("Introduza y2"))

## Valor que armazena a distância
dist = ((x2-x1)**2) + ((y2-y1)**2) ** 0.5

print(f"A distancia é igual a {dist}")
