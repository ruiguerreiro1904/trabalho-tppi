##
# \brief Média das notas de 4 disciplinas
# \details Aula 2
# \author Rui Guerreiro  
# \date 27/11/2019
# \bug 0 bugs
# \version 0.0    

## Valor que armazena a nota de Matemática introduzido pelo utilizador
nota1 = float(input("Introduza a nota de Matemática"))
## Valor que armazena a nota de Português introduzido pelo utilizador
nota2 = float(input("Introduza a nota de Português"))
## Valor que armazena a nota de Inglês introduzido pelo utilizador
nota3 = float(input("Introduza a nota de Inglês"))
## Valor que armazena a nota de Geografia introduzido pelo utilizador
nota4 = float(input("Introduza a nota de Geografia"))

## Guarda a média das notas na variável
media = (nota1 + nota2 + nota3 + nota4) /4
if media >= 9.50:
    print(f"O aluno foi aprovado com a media de {media} valores")
else:
    print(f"O aluno nao foi aprovado pois obteve a media de {media}valores")
