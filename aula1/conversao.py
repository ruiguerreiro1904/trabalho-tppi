##
# \brief Conversão de dólares para euros
# \details Aula 1  
# \author Rui Guerreiro  
# \date 27/11/2019
# \bug 0 bugs
# \version 0.0    

##Representa a variável euros introduzida pelo utilizador
euro = float(input("Introduza o valor em euros"))

##Representa o valor de 1 euro em dólares
valor=1.17

##Representa o valor do euro * valor
dolar = euro * valor

print("a conversao é",dolar)
