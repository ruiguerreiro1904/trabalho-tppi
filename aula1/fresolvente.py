##
# \brief Fórmula Resolvente    
# \details Aula 1  
# \author Rui Guerreiro  
# \date 27/11/2019
# \bug 0 bugs
# \version 0.0    

##Importação da biblioteca Random
from random import *

##Atribuição de um valor float entre 0 e 10
a = uniform(0,10)
print(a)

##Atribuição de um valor float entre 5 e 20
b = uniform(5,20)
print(b)

##Atribuição de um valor float entre 5 e 20
c = uniform(5,20)
print(c)

##d corresponde à parte que está dentro da raíz da fórmula resolvente
d = b*b - 4.0*a*c

##rq faz a raíz de d
rq = d**0.5

##x1 corresponde ao primeiro resultado
x1 = (-b + rq) / 2.0*a

##x2 corresponde ao segundo resultado
x2 = (-b - rq) / 2.0*a
if d < 0:
    print("não existem raízes reais")
else:
    print(x1)
    print(x2)
