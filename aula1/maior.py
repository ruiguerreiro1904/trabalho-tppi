##
# \brief Simulação básica de uma máquina  
# \details Aula 1  
# \author Rui Guerreiro  
# \date 27/11/2019
# \bug 0 bugs
# \version 0.0     

##Representa o primeiro valor introduzido pelo utilizador
num1 = int(input("Introduza o número 1"))
##Representa o segundo valor introduzido pelo utilizador
num2 = int(input("Introduza o número 2"))
##Representa o terceiro valor introduzido pelo utilizador
num3 = int(input("Introduza o número 3"))

if num1 >= num2:
    if num1 >= num3:
        print("O número maior é",num1)
    elif num2>= num3:
        print("O número maior é",num2)
else:
    print("O número maior é",num3)
