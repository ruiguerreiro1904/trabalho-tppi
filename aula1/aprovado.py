##
# \brief Aluno aprovado ou reprovado  
# \details Aula 1  
# \author Rui Guerreiro  
# \date 27/11/2019
# \bug 0 bugs
# \version 0.0    


##Nota do aluno
nota = float(input("Introduza a nota do aluno"))

# Se a nota for maior ou igual que 9.5 imprime "O aluno foi aprovado"
if nota >= 9.50:
    print ("O aluno foi aprovado")

else:
    print("O aluno nao foi aprovado")
