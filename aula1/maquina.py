##
# \brief Simulação básica de uma máquina  
# \details Aula 1  
# \author Rui Guerreiro  
# \date 27/11/2019
# \bug 0 bugs
# \version 0.0     


##Representa a instrução introduzida pelo utilizador
#
#Se escolher:  
#-L -> Vai ligar a máquina  
#-D -> Vai desligar a máquina  
#-F -> Vai furar   
opcao = (input("Introduza L/D/F"))

if opcao == 'L' or opcao == 'l':
    print("Ligar")
elif opcao == 'D' or opcao == 'd':
    print("Desligar")
elif opcao == 'F' or opcao == 'f':
    print("Furar")
else:
    print("opção inválida")
