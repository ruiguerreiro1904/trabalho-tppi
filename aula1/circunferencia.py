##Representa o valor do raio introduzido pelo utilizador
raio = float(input("Introduza o raio"))

##Valor do Pi
pi=3.1415

##Representa a área da circunferência de acordo com o raio introduzido pelo utilizador
area = pi * (raio * raio)

##Representa o perímetro do círculo
perimetro = 2 * pi * raio

print("A área da circunferencia é",area)
print("O perimetro da circunferencia é",perimetro)
