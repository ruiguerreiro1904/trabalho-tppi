##
# \brief Área do quadrado ou retângulo?  
# \details Aula 1  
# \author Rui Guerreiro  
# \date 27/11/2019
# \bug 0 bugs
# \version 0.0    

##Variável que representa o lado 1
lado1 = float(input("Introduza o lado1"))
##Variável que representa o lado 2
lado2 = float(input("Introduza o lado2"))

##Representa a área dos dois lados introduzidos pelo utilizador
area = lado1 * lado2

# Se os lados forem iguais imprime a área do quadrado, senão imprime a área do retângulo

if lado1 == lado2:
    print("area do quadrado=",area)
else:
    print("area do retangulo",area)
