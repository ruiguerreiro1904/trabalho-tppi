# --------- Global Variables -----------

# Tabuleiro do jogo

## Variável que armazena o tabuleiro de jogo
board = ["-", "-", "-",
         "-", "-", "-",
         "-", "-", "-"]


## Variável que determina se o jogo acaba
# game_still_going = True -> Jogo ainda não acabou
# game_still_going = False -> Jogo acabou  
game_still_going = True

## Variável que armazena o vencedor  
# Valor default é None pois ainda não há vencedor.
winner = None

## Variável utilizada para definir e cambiar entre jogadores  
# O jogador X começa a jogar.
current_player = "X"
# ------------- Functions ---------------

## Função principal que é utilizada para jogar o jogo.  
# \param Não tem parâmetros.
# \return Retorna o jogo  
 
def play_game():
    # Mostrar o tabuleiro
    display_board()
    pontos = int(open('./winner.txt', 'r').readline().split(':')[1].strip())
    print(f'Tens {pontos} pontos!')

    # Loop until the game stops (winner or tie)
    while game_still_going:
        # Handle a turn
        handle_turn(current_player)

        check_if_game_over()

        flip_player()

    if winner == "X" or winner == "O":
        print(winner + " ganhou.")
    elif winner == None:
        print("Tie.")

## Função utilizada para mostrar o campo atual.  
# \param Não tem parâmetros.
# \return Retorna o tabuleiro do jogo atual.  
def display_board():
    print("\n")
    print(board[0] + " | " + board[1] + " | " + board[2] + "     1 | 2 | 3")
    print(board[3] + " | " + board[4] + " | " + board[5] + "     4 | 5 | 6")
    print(board[6] + " | " + board[7] + " | " + board[8] + "     7 | 8 | 9")
    print("\n")

## Função utilizada para gerir o jogador atual  
# \param Recebe como parâmetro a variável que contém o jogador
# \return Retorna o tabuleiro do jogo atual.
# \sa Faz uso da função display_board().  
def handle_turn(player):
    # Get position from player
    print(f"Vez do {player}")
    position = input("Escolhe uma posição de 1-9: ")

    # Whatever the user inputs, make sure it is a valid input, and the spot is open
    valid = False
    while not valid:

        while position not in ["1", "2", "3", "4", "5", "6", "7", "8", "9"]:
            position = input("Escolhe uma posição de 1-9: ")

        position = int(position) - 1

        # Then also make sure the spot is available on the board
        if board[position] == "-":
            valid = True
        else:
            print("Essa posição não está disponivel. Escolhe outra.")

    # Put the game piece on the board
    board[position] = player

    display_board()


## Função utilizada para verificar se o jogo acabou
# \param Não tem parâmetros.  
# \return Não retorna nada.  
# \sa Faz uso das funções check_for_winner() e check_for_tie().  
def check_if_game_over():
    check_for_winner()
    check_for_tie()

## Função utilizada para guardar os pontos num ficheiro.  
# \param Não tem parâmetros.
# \return Não retorna nada.    
def savePoints():
    pontos = int(open('./winner.txt', 'r').readline().split(':')[1].strip())
    pontos += 10
    with open('./winner.txt', 'w+') as f:
        f.write(f'pontos: {pontos}')

    print(open('./winner.txt', 'r').readline())
    f.close()

## Função utilizada para verificar se há jogadas vencedoras.   
# \param Não tem parâmetros.  
# \return Não retorna nada.  
# \sa Faz uso das subfunções da função check_rows().  
def check_for_winner():
    # Set global variables
    global winner

    # Check if there was a winner anywhere
    row_winner = check_rows()
    column_winner = check_columns()
    diagonal_winner = check_diagonals()
    # Get the winner
    if row_winner:
        savePoints()
        winner = row_winner
    elif column_winner:
        savePoints()
        winner = column_winner
    elif diagonal_winner:
        savePoints()
        winner = diagonal_winner
    else:
        winner = None


## Função utilizada para verificar se há jogadas vencedoras.
## Verifica as verticais.  
# \param Não tem parâmetros.  
# \return Retorna a jogada vencedora.        
def check_rows():
    # Set global variables
    global game_still_going
    # Check if any of the rows have all the same value (and is not empty)
    row_1 = board[0] == board[1] == board[2] != "-"
    row_2 = board[3] == board[4] == board[5] != "-"
    row_3 = board[6] == board[7] == board[8] != "-"
    # If any row does have a match, flag that there is a win
    if row_1 or row_2 or row_3:
        game_still_going = False
    # Return the winner
    if row_1:
        return board[0]
    elif row_2:
        return board[3]
    elif row_3:
        return board[6]
        # Or return None if there was no winner
    else:
        return None


## Função utilizada para verificar se há jogadas vencedoras.
## Verifica as horizontais.  
# \param Não tem parâmetros.  
# \return Retorna a jogada vencedora. 
def check_columns():
    # Set global variables
    global game_still_going
    # Check if any of the columns have all the same value (and is not empty)
    column_1 = board[0] == board[3] == board[6] != "-"
    column_2 = board[1] == board[4] == board[7] != "-"
    column_3 = board[2] == board[5] == board[8] != "-"
    # If any row does have a match, flag that there is a win
    if column_1 or column_2 or column_3:
        game_still_going = False
    # Return the winner
    if column_1:
        return board[0]
    elif column_2:
        return board[1]
    elif column_3:
        return board[2]
        # Or return None if there was no winner
    else:
        return None


## Função utilizada para verificar se há jogadas vencedoras.
## Verifica as diagonais.  
# \param Não tem parâmetros.  
# \return Retorna a jogada vencedora. 
def check_diagonals():
    # Set global variables
    global game_still_going
    # Check if any of the columns have all the same value (and is not empty)
    diagonal_1 = board[0] == board[4] == board[8] != "-"
    diagonal_2 = board[2] == board[4] == board[6] != "-"
    # If any row does have a match, flag that there is a win
    if diagonal_1 or diagonal_2:
        game_still_going = False
    # Return the winner
    if diagonal_1:
        return board[0]
    elif diagonal_2:
        return board[2]
    # Or return None if there was no winner
    else:
        return None


## Função utilizada para verificar se há empate.    
# \param Não tem parâmetros.  
# \return Retorna a True ou False (Empate ou não empate).  
def check_for_tie():
    # Set global variables
    global game_still_going
    # If board is full
    if "-" not in board:
        game_still_going = False
        return True
    # Else there is no tie
    else:
        return False


## Função utilizada para cambiar entre jogadores.    
# \param Não tem parâmetros.  
# \return Não retorna nada.  
def flip_player():
    # Global variables we need
    global current_player
    # If the current player was X, make it O
    if current_player == "X":
        current_player = "O"
    # Or if the current player was O, make it X
    elif current_player == "O":
        current_player = "X"


# ------------ Start Execution -------------
# Play a game of tic tac toe
play_game()
