import random
import re
from string import ascii_lowercase

## Função utilizada para gerir as alterações no tabuleiro de jogo.    
# \param Recebe como parâmetro o tamanho da grid, start e o número de minas.
# \return Retorna o tabuleiro de jogo e as minas.    
def setupgrid(gridsize, start, numberofmines):
    emptygrid = [['0' for i in range(gridsize)] for i in range(gridsize)]

    mines = getmines(emptygrid, start, numberofmines)

    for i, j in mines:
        emptygrid[i][j] = 'X'

    grid = getnumbers(emptygrid)

    return (grid, mines)


## Função utilizada para mostrar o tabuleiro de jogo.    
# \param Recebe como parâmetro a grid.
# \return Retorna o tabuleiro de jogo.
# \sa setupgrid()
def showgrid(grid):
    gridsize = len(grid)

    horizontal = '   ' + (4 * gridsize * '-') + '-'

    # Imprime a coluna numerada
    toplabel = '     '

    for i in ascii_lowercase[:gridsize]:
        toplabel = toplabel + i + '   '

    print(toplabel + '\n' + horizontal)

    # Imprime a linha numerada da esquerda
    for idx, i in enumerate(grid):
        row = '{0:2} |'.format(idx + 1)

        for j in i:
            row = row + ' ' + j + ' |'

        print(row + '\n' + horizontal)

    print('')

## Função utilizada para gerar dois valores aleatórios.    
# \param Recebe como parâmetro a grid.  
# \return Retorna os dois valores aleatórios inteiros (a e b).
# \sa Faz uso da biblioteca importada (random)
def getrandomcell(grid):
    gridsize = len(grid)

    a = random.randint(0, gridsize - 1)
    b = random.randint(0, gridsize - 1)

    return (a, b)


## Função utilizada para saber o conteúdo das células ao redor.    
# \param Recebe como parâmetro a grid e as coordenadas da célula (rowno e colno).
# \return Retorna um vetor com as coordenadas das células vizinhas. 
def getneighbors(grid, rowno, colno):
    gridsize = len(grid)
    neighbors = []

    for i in range(-1, 2):
        for j in range(-1, 2):
            if i == 0 and j == 0:
                continue
            elif -1 < (rowno + i) < gridsize and -1 < (colno + j) < gridsize:
                neighbors.append((rowno + i, colno + j))

    return neighbors


## Função utilizada para criar as minas.    
# \param Recebe como parâmetro a grid, start e o número de minas.
# \return Retorna um vetor com as minas
def getmines(grid, start, numberofmines):
    mines = []
    neighbors = getneighbors(grid, *start)

    for i in range(numberofmines):
        cell = getrandomcell(grid)
        while cell == start or cell in mines or cell in neighbors:
            cell = getrandomcell(grid)
        mines.append(cell)

    return mines


## Função utilizada para saber os valores das células.    
# \param Recebe como parâmetro a grid.  
# \return Retorna a grid com os valores.
def getnumbers(grid):
    for rowno, row in enumerate(grid):
        for colno, cell in enumerate(row):
            if cell != 'X':
                # Gets the values of the neighbors
                values = [grid[r][c] for r, c in getneighbors(grid,
                                                              rowno, colno)]

                # Conta quantas são minas
                grid[rowno][colno] = str(values.count('X'))

    return grid


## Função utilizada para mostrar as células.    
# \param Recebe como parâmetro a grid no seu estado original e atual e as coordenadas da célula (rowno e colno).
# \return Não retorna nada.  
def showcells(grid, currgrid, rowno, colno):
    # Exit function if the cell was already shown
    if currgrid[rowno][colno] != ' ':
        return

    # Mostra a célula atual
    currgrid[rowno][colno] = grid[rowno][colno]

    # Get the neighbors if the cell is empty
    if grid[rowno][colno] == '0':
        for r, c in getneighbors(grid, rowno, colno):
            # Repeat function for each neighbor that doesn't have a flag
            if currgrid[r][c] != 'F':
                showcells(grid, currgrid, r, c)



## Função utilizada para jogar de novo.    
# \param Não recebe parâmetros.
# \return Retorna a escolha.
def playagain():
    choice = input('Quer jogar de novo? (y/n): ')

    return choice.lower() == 'y'



## Função utilizada para direcionar o input do jogador    
# \param Recebe como parâmetro a string que contém a mensagem do utilizador, o tamanho da grid e a mensagem de ajuda.  
# \return Retorna a célula, a bandeira e a mensagem.
def parseinput(inputstring, gridsize, helpmessage):
    cell = ()
    flag = False
    message = "Célula inválida " + helpmessage

    pattern = r'([a-{}])([0-9]+)(f?)'.format(ascii_lowercase[gridsize - 1])
    validinput = re.match(pattern, inputstring)

    if inputstring == 'help':
        message = helpmessage

    elif validinput:
        rowno = int(validinput.group(2)) - 1
        colno = ascii_lowercase.index(validinput.group(1))
        flag = bool(validinput.group(3))

        if -1 < rowno < gridsize:
            cell = (rowno, colno)
            message = ''

    return {'Célula': cell, 'flag': flag, 'Mensagem': message}


## Função principal para jogar o jogo.      
# \param Não recebe parâmetros.
# \return Retorna o jogo.
def playgame():
    gridsize = 9
    numberofmines = 10

    currgrid = [[' ' for i in range(gridsize)] for i in range(gridsize)]

    grid = []
    flags = []

    helpmessage = ("Escreve a coluna e a linha (ex. a2). "
                   "Para adicionar ou remover uma bandeira, adiciona f ao anterior (ex. a5f).")

    showgrid(currgrid)
    print(helpmessage + " Escreve 'help' para leres esta mensagem de novo\n")

    while True:
        minesleft = numberofmines - len(flags)
        prompt = input('Introduz a célula ({} minas restantes): '.format(minesleft))
        result = parseinput(prompt, gridsize, helpmessage + '\n')

        message = result['mensagem']
        cell = result['Célula']

        if cell:
            print('\n\n')
            rowno, colno = cell
            currcell = currgrid[rowno][colno]
            flag = result['flag']

            if not grid:
                grid, mines = setupgrid(gridsize, cell, numberofmines)

            if flag:
                # Adiciona uma bandeira se a célula já existe
                if currcell == ' ':
                    currgrid[rowno][colno] = 'F'
                    flags.append(cell)
                # Remove a bandeira se já existir uma
                elif currcell == 'F':
                    currgrid[rowno][colno] = ' '
                    flags.remove(cell)
                else:
                    message = 'Não podes meter a bandeira aí'

            # If there is a flag there, show a message
            elif cell in flags:
                message = 'Já está aqui uma bandeira!'

            elif grid[rowno][colno] == 'X':
                print('Game Over\n')
                showgrid(grid)
                if playagain():
                    playgame()
                return

            elif currcell == ' ':
                showcells(grid, currgrid, rowno, colno)

            else:
                message = "That cell is already shown"

            if set(flags) == set(mines):
                print(
                    'You Win. ')
                showgrid(grid)
                if playagain():
                    playgame()
                return

        showgrid(currgrid)
        print(message)

playgame()
